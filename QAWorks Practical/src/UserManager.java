import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class UserManager implements UserInterface {

	private List<User> UserList = new ArrayList<User>();
	private Random random = new Random();

	public void UserAdd(User user) {
		UserList.add(user);
	}

	public void UserDelete(String firstName, String surname) {
		Iterator <User> it = UserList.iterator();
		while(it.hasNext()){
			User user = it.next();
			if(user.getFirstName().equals(firstName)&& user.getSurname().equals(surname)){
				it.remove();
			}
		}
	}

	public void UserList() {
		for (int i = 0; i < UserList.size(); i++) {
			System.out.println(UserList.get(i));
		}
	}

	public void RandomUserGet() {
		int index = random.nextInt(UserList.size());
		System.out.println(UserList.get(index));
	}

	public void Serialize() {
		XMLEncoder encoder = null;
		try {
			encoder = new XMLEncoder(new BufferedOutputStream(new FileOutputStream("XMLDatabase.xml")));
		} catch (FileNotFoundException fileNotFound) {
			System.out.println("ERROR: While Creating or Opening the File XMLDatabase.xml");
		}
		encoder.writeObject(UserList);
		encoder.close();
	}

	public void Deserialize() {
		XMLDecoder decoder = null;
		try {
			decoder = new XMLDecoder(new BufferedInputStream(new FileInputStream("XMLDatabase.xml")));
		} catch (FileNotFoundException e) {
			System.out.println("ERROR: File XMLDatabase.xml not found");
		}
		decoder.readObject();

	}

}
