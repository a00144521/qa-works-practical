
public class User {

	private String FirstName, Surname;

	public User(String firstName, String surname) {
		this.FirstName = firstName;
		this.Surname = surname;
	}

	public User() {
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String firstName) {
		FirstName = firstName;
	}

	public String getSurname() {
		return Surname;
	}

	public void setSurname(String surname) {
		Surname = surname;

	}

	public String toString() {
		return FirstName + " " + Surname;
	}

}
