import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		UserManager userManager = new UserManager();
		userManager.Deserialize();
		Scanner scanner = new Scanner(System.in);
		int choice = 1;

		while (choice != 5) {
			System.out.println("Please select and option.");
			System.out.println("1.Add User");
			System.out.println("2.Delete User");
			System.out.println("3.Print All Users");
			System.out.println("4.Select Random User");
			System.out.println("5.Exit");

			choice = scanner.nextInt();

			if (choice == 1) {
				System.out.println("Enter the users first name:");
				String firstName = scanner.next();
				System.out.println("Enter the users surname:");
				String surname = scanner.next();
				User user = new User();
				user.setFirstName(firstName);
				user.setSurname(surname);
				userManager.UserAdd(user);

			} else if (choice == 2) {
				System.out.println("Enter the users first name:");
				String firstName = scanner.next();
				System.out.println("Enter the users surname:");
				String surname = scanner.next();
				userManager.UserDelete(firstName, surname);

			} else if (choice == 3) {
				userManager.UserList();

			} else if (choice == 4) {
				userManager.RandomUserGet();
			}
		}
		userManager.Serialize();

	}
}
